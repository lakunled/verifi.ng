module.exports = function(Schema){
    var AddressDefinition = {
        number: String,
        street: String,
        town: String,
        lga: String,
        state: String
    };

    return new Schema(AddressDefinition);
};