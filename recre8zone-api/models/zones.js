var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var AddressSchema = require('./subschema/address')(Schema);

var zonesSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    contactNumber: {
        type: String,
        required: true
    },
    openingHour: {
        type: String,
        required: true
    },
    closingHour: {
        type: String,
        required: true
    },
    contactPerson: {
        type: String,
        required: true
    },
    contactEmail: {
        type: String,
        required: true
    }
}, {
    timestamps: true,
    collection: 'zones'
});

var Zones = mongoose.model("Zone", zonesSchema);
module.exports = Zones;