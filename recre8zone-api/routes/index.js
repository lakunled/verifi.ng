'use strict'
var express = require('express');
var passport = require('passport');
var app = express.Router();

var Zone = require('../models/zones');


app.get('/zones', function (req, res) {
    Zone.find({}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "Error occurred while fetching all Zones"});
        }
        res.status(200).json({status:true, data:data});
    });
});

app.post('/zone', function (req, res) {

    var data = new Zone({
        name: req.body.name,
        location: req.body.location,
        category: req.body.category,
        description: req.body.description,
        contactNumber: req.body.contactNumber,
        openingHour: req.body.openingHour,
        closingHour: req.body.closingHour,
        contactPerson: req.body.contactPerson,
        contactEmail: req.body.contactEmail
    });

    data.save(function (err) {
        if (err){
            return res.status(400).json({status: false, message: "Error while adding new Zone"});
        }
        res.status(200).json({status: true, message: "Zone added successfully"});
    });


});

app.get('/getZonesByCategory', function (req, res) {
    var category = req.body.category;
    Zone.find({category:"Gym"}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "Error occurred while fetching "+category+" category"});
        }
        res.status(200).json({status:true, data:data});
    });
});

app.get('/getASingleZone', function (req, res) {
    var id = req.body.id;
    Zone.findOne({_id:id}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "Zone not found"});
        }
        res.status(200).json({status:true, data:data});
    });
});

app.get('/profile', isLoggedIn, function(req, res) {
    res.render('profile.ejs', {
        user : req.user
    });
});

app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/profile',
    failureRedirect : '/signup',
    failureFlash : true
}));

app.post('/login', passport.authenticate('local-login', {
    successRedirect : '/profile',
    failureRedirect : '/login',
    failureFlash : true
}));

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}



module.exports = app;
