var LocalStrategy = require('passport-local').Strategy;

var User = require('../app/models/user');

// expose this function to our app using module.exports
module.exports = function (passport) {

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });


    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, firstname, lastname, phonenumber, done) {

            process.nextTick(function () {


                // checks if the user trying to login already exists
                User.findOne({'local.email': email}, function (err, user) {
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, {status: false, message: "Email already exist"});
                    } else {

                        var newUser = new User();

                        // set the user's local credentials
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);
                        newUser.local.firstname = firstname;
                        newUser.local.lastname = lastname;
                        newUser.local.phonenumber = phonenumber;

                        // save the user
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }

                });

            });

        }
    ));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            User.findOne({'local.email': email}, function (err, user) {
                if (err)
                    return done(err);
                if (!user)
                    return done(null, false, {message: 'No user found.'});
                if (!user.validPassword(password))
                    return done(null, false, {message: 'Oops! Wrong password.'});
                return done(null, user);
            });
        }
    ));


};