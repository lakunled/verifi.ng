var Report = require('../models/report');
var jwt = require('jsonwebtoken');
var env = require('../.env');
var nodemailer = require('nodemailer');
var sendmail = require('sendmail')();

module.exports.getAllReports = function (req, res) {
    Report.find({}, function (err, report) {
        if (err) throw err;
        res.json(report);
    });
}

module.exports.postReport = function (req, res) {
    var token = req.headers.jwt;
    var decoded = jwt.verify(token, "0129384765YTRUEIWOQlaskdjfhgVBCNMXZ");

    var user_id = decoded._doc._id;
    var senderMail = decoded._doc.email;
    var title = req.body.title;
    var content = req.body.content;
    var email = req.body.email;
    var company = req.body.company_id;

    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: env.smtpUsername, // Your email id
            pass: env.smtpPassword // Your password
        }
    });

    for (var i = 0; i < email.length; i++) {
        var mailOptions = {
            from: 'OlakunleDosunmu@gmail.com', // sender address
            to: email[i], // list of receivers
            subject: title, // Subject line
            html: content // You can choose to send an HTML body instead
        };
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
            }else{
                console.log("sent");
            };
        });
    }
    var report = new Report({
        title: title,
        content: content,
        email: email,
        user_id: user_id,
        company_id: company
    });

    report.save(function (err) {
        if (err) {
            console.log(err);
            return res.status(400).json({status: false, message: "Error while saving New report"});
        }
        res.status(200).json({status: true, message: "Report sent successfully"});
    });

};

module.exports.getByUserId = function (req, res) {
    Report.find({user_id:req.body.user_id}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "News article not found. Please login"});
        }
        res.status(200).json(data);
    })
};

module.exports.getByCompanyId = function (req, res) {
    Report.find({company_id:company_id}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "News article not found. Please login"});
        }
        res.status(200).json(data);
    })
}