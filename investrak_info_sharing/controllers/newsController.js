var News = require('../models/news');
var jwt    = require('jsonwebtoken');

module.exports.getAllNews = function (req, res) {
    News.find({}, function (err, news) {
        if (err) throw err;
        res.json(news);
    });
}

module.exports.postNews = function (req, res) {
    var token = req.headers.jwt;
    var decoded = jwt.verify(token, "0129384765YTRUEIWOQlaskdjfhgVBCNMXZ");

    var user_id = decoded._doc._id;
    var title = req.body.title;
    var body = req.body.body;
    var company_id = req.body.company_id;

    var data = new News({
        title: title,
        body: body,
        user_id: user_id,
        company_id: company_id
    });

    data.save(function (err) {
        if (err) {
            console.log(err);
            return res.status(400).json({status: false, message: "Error while saving New article"});
        }
        res.status(200).json({status: true, message: "News added successfully"});
    });
};

module.exports.getSingleNews = function (req, res) {
    News.findById(req.params.newsId, function (err, news) {
        if (err){
            return res.status(400).json({status: false, message: "News article not found"});
        }
        res.json(news);
    });
};

module.exports.getByUserId = function (req, res) {
    News.find({user_id:req.body.user_id}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "News article not found. Please login"});
        }
        res.status(200).json(data);
    })
};

module.exports.getByCompanyId = function (req, res) {
    News.find({company_id:req.body.company_id}, function (err, data) {
        if (err){
            return res.status(400).json({status: false, message: "News article not found. Please login"});
        }
        res.status(200).json(data);
    })
}

module.exports.deleteNews = function (req, res) {
    News.findByIdAndRemove(req.params.newsId, function (err, resp) {
        if (err) throw err;
        res.json({
            status: true,
            news: resp
        });
    });
}

