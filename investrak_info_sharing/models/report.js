var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reportSchema = new Schema({
        title: {
            type: String,
            required: true
        },
        content: {
            type: String,
            required: true
        },
        email: {
            type: Array,
            required: true
        },
        user_id: {
            type: String,
            required: true
        },
        company_id: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    });

var Report = mongoose.model('Report', reportSchema);

module.exports = Report;