var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

var newsController = require('../controllers/newsController');
var reportController = require('../controllers/reportController');

var News = require('../models/news');
var Report = require('../models/report');

var newsRouter = express.Router();
newsRouter.use(bodyParser.json());

newsRouter.route('/')
    .get(validateAuthentication, newsController.getByUserId)
    .post(validateAuthentication, newsController.postNews);

newsRouter.route('/:newsId')
    .get(validateAuthentication, newsController.getSingleNews)
    .delete(validateAuthentication, newsController.deleteNews);

// newsRouter.route('/getByUserId')
//     .get(validateAuthentication, newsController.getByUserId);

newsRouter.route('/getByCompanyId')
    .get(validateAuthentication, newsController.getByCompanyId);

newsRouter.route('/postReport')
    .post(validateAuthentication, reportController.postReport);

newsRouter.route('/getReportByUserId')
    .get(validateAuthentication, reportController.getByUserId);

newsRouter.route('getReportByCompanyId')
    .get(validateAuthentication, reportController.getByCompanyId);


function validateAuthentication(req, res, next){
    if(req.headers.jwt){
        try{
            var decoded = jwt.verify(req.headers.jwt, "0129384765YTRUEIWOQlaskdjfhgVBCNMXZ");
            next();
        } catch (err){
            console.log(err);
            res.json({status:"error", message:"You have not logged in"});
        }
    } else {
        //req.flash('error_msg','You are not logged in');
        res.json({status:"error", message:"Unauthorised"});
    }
}




module.exports = newsRouter;