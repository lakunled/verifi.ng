'use strict';
const Hapi = require("hapi");
const server = new Hapi.Server();
const Path = require('path');
var route = require('./routes/route');
var MongoClient = require('mongodb').MongoClient;


var mongoDbConnectionString ='mongodb://localhost:27017/gamsole';

MongoClient.connect(mongoDbConnectionString, function(err, db) {
    console.log("Connected correctly to server");
    addRoutes(server, db);
    startServer(server);
});

server.connection({
    "port": 4060
});

function addRoutes(server, db) {
    route(server, db);
}

function startServer(server) {
    server.start(function () {
        console.log('Server running at: ' + server.info.uri);
        server.log('info', 'Server running at: ' + server.info.uri);
    });
}