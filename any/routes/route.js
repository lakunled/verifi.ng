
var ObjectID = require('mongodb').ObjectID;

var collectionAnimations = "animations";
var collectionGames = "games";
var collectionComics = "comics";



var gamsole = function (server, mongodb) {

    server.route({
        method: "POST",
        path: "/animations",
        handler: function (request, reply) {
            var animationsCollection = mongodb.collection(collectionAnimations);

            var payload = request.payload;

            var reponse = "";

            for (var x in payload) {
                if (payload[x] == "") {
                    reponse = x + " cannot be null";
                    break;
                }
                else {
                    animationsCollection.insert(payload, function (err, animation) {
                        if (err) {
                            reponse = "Error occurred while trying to insert a new animation";
                            return;
                        }
                        else {
                        }
                    });
                    reponse = "Inserted a new animation";
                }
            }
            reply(reponse);
        }
    });

    server.route({
        method: "GET",
        path: "/animations",
        handler: function (request, reply) {
            var animationsCollection = mongodb.collection(collectionAnimations);

            animationsCollection.find({}).toArray(function (err, animations) {
                if (err) {
                    reply("Error occurred while trying to fetch all animations");
                }
                reply(animations);
            });
        }
    });

    server.route({
        method: "GET",
        path: "/animation",
        handler: function (request, reply) {
            var animationsCollection = mongodb.collection(collectionAnimations);
            var id = request.payload.id;

            animationsCollection.findOne({_id: new ObjectID(id)}, function (err, animation) {
                if (err) {
                    reply("Error occurred while trying to fetch an animation");
                    return;
                }
                reply(animation);
            })
        }
    })

    server.route({
        method: "POST",
        path: "/deleteanimation",
        handler: function (request, reply) {
            var animationsCollection = mongodb.collection(collectionAnimations);

            var id = request.payload.id;

            animationsCollection.deleteOne({_id: new ObjectID(id)}, function (err, result) {
                if (err) {
                    reply("Error occurred while trying to delete an animation");
                    return;
                }
                reply("Removed the document with object id" + id);
            });
        }
    });

    server.route({
        method: "POST",
        path: "/comics",
        handler: function (request, reply) {
            var comicsCollection = mongodb.collection(collectionComics);

            var payload = request.payload;

            var reponse = "";

            for (var x in payload) {
                if (payload[x] == "") {
                    reponse = x + " cannot be null";
                    break;
                }
                else {
                    comicsCollection.insert(payload, function (err, result) {
                        if (err) {
                            reponse = "Error occurred while trying to insert a new comic";
                            return;
                        }
                        else {
                        }
                    });
                    reponse = "Inserted a new comic";
                }
            }
            reply(reponse);
        }
    });

    server.route({
        method: "GET",
        path: "/comics",
        handler: function (request, reply) {
            var comicsCollection = mongodb.collection(collectionComics);

            comicsCollection.find({}).toArray(function (err, comics) {
                if (err) {
                    reply("Error occurred while trying to fetch all comics");
                }
                reply(comics);
            });
        }
    });

    server.route({
        method: "GET",
        path: "/comic",
        handler: function (request, reply) {
            var comicsCollection = mongodb.collection(collectionComics);
            var id = request.payload.id;

            comicsCollection.findOne({_id: new ObjectID(id)}, function (err, comic) {
                if (err) {
                    reply("Error occurred while trying to fetch a comic");
                    return;
                }
                reply(comic);
            })
        }
    })

    server.route({
        method: "POST",
        path: "/deletecomic",
        handler: function (request, reply) {
            var comicsCollection = mongodb.collection(collectionComics);

            var id = request.payload.id;

            comicsCollection.deleteOne({_id: new ObjectID(id)}, function (err, comic) {
                if (err) {
                    reply("Error occurred while trying to delete a comic");
                    return;
                }
                reply("Removed the document with object id" + id);
            });
        }
    });


    server.route({
        method: "POST",
        path: "/games",
        handler: function (request, reply) {
            var gamesCollection = mongodb.collection(collectionGames);

            var payload = request.payload;

            var reponse = "";

            for (var x in payload) {
                if (payload[x] == "") {
                    reponse = x + " cannot be null";
                    break;
                }
                else {
                    gamesCollection.insert(payload, function (err, game) {
                        if (err) {
                            reponse = "Error occurred while trying to insert a new game";
                            return;
                        }
                        else {
                        }
                    });
                    reponse = "Inserted a new game";
                }
            }
            reply(reponse);
        }
    });

    server.route({
        method: "GET",
        path: "/games",
        handler: function (request, reply) {
            var gamesCollection = mongodb.collection(collectionGames);

            gamesCollection.find({}).toArray(function (err, games) {
                if (err) {
                    reply("Error occurred while trying to fetch all games");
                }
                reply(games);
            });
        }
    });

    server.route({
        method: "GET",
        path: "/game",
        handler: function (request, reply) {
            var gamesCollection = mongodb.collection(collectionGames);
            var id = request.payload.id;

            gamesCollection.findOne({_id: new ObjectID(id)}, function (err, game) {
                if (err) {
                    reply("Error occurred while trying to fetch a game");
                    return;
                }
                reply(game);
            })
        }
    })

    server.route({
        method: "POST",
        path: "/deletegame",
        handler: function (request, reply) {
            var gamesCollection = mongodb.collection(collectionGames);

            var id = request.payload.id;

            gamesCollection.deleteOne({_id: new ObjectID(id)}, function (err, game) {
                if (err) {
                    reply("Error occurred while trying to delete a game");
                    return;
                }
                reply("Removed the document with object id" + id);
            });
        }
    });
}

module.exports = gamsole;