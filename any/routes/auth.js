var collectionUsers = "users";

var auth = function (server, mongodb) {
    server.route({
        method: "POST",
        path: "/register",
        handler: function (request, reply) {
            var usersCollections = mongodb.collection(collectionUsers);

            var payload = request.payload;
            var account_number = payload.account_number;
            var email = payload.email;
            var password = payload.password;

            if (account_number || email || password == "") {
                reply("All fiels required");
                return;
            }
            else{
                usersCollections.findOne({email:email}, function (err, data) {
                    if (err){
                        reply("error occurred while trying to confirm if user already signed up");
                        return;
                    }
                    else {
                        if (data != null){
                            return("Email Address already exist. Please signup using another Email Address");
                        }
                        usersCollections.insert(payload, function (err, userDetails) {
                            if (err) {
                                reply("error occurred while to insert the user details");
                                return;
                            }
                            reply("New User Details saved");
                        });
                    }
                });
            }
        }
    });

    server.route({
        method: "POST",
        path: "/login",
        handler: function (request, reply) {
            var usersCollections = mongodb.collection(collectionUsers);

            var payload = request.payload;

            var email = payload.email;
            var password = payload.password;

            usersCollections.findOne({}, function (err, user) {

            });
        }
    });

    
}

module.exports = auth;