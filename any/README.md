
/animations POST insert a new animation
/animations GET get all animations
/animation GET payload:id retrieve a single animation
/deleteanimation POST payload:id delete a single animation

/games POST insert a new game
/games GET get all games
/game GET payload:id retrieve a single game
/deletegame POST payload:id delete a single game

/comics POST insert a new comic
/comics GET get all comics
/comic GET payload:id retrieve a single comic
/deletecomic POST payload:id delete a single comic